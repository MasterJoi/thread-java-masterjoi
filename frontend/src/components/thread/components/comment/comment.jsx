import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon, Label } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';

import PropTypes from 'prop-types';
import styles from './styles.module.scss';
import { IconName } from '../../../../common/enums/components/icon-name.enum';

const Comment = ({ comment, onCommentLike, onCommentDislike }) => {
  const {
    id,
    body,
    user,
    likeCount,
    dislikeCount,
    createdAt
  } = comment;
  const date = getFromNowTime(createdAt);

  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{date}</CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired
};

export default Comment;
