import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showOtherPosts, setShowOtherPosts] = React.useState(false);
  const [showLikedPosts, setShowLikedPosts] = React.useState(false);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleOtherPostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadOtherPosts(filtersPayload));
  };

  const handleLikedPostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadLikedUserPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload,
      showOwnPosts, showOtherPosts, showLikedPosts));
  };

  // eslint-disable-next-line no-unused-vars
  const toggleShowOwnPosts = () => {
    setShowOtherPosts(false);
    setShowLikedPosts(false);
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOtherPosts = () => {
    setShowOwnPosts(false);
    setShowLikedPosts(false);
    setShowOtherPosts(!showOtherPosts);
    postsFilter.userId = showOtherPosts ? undefined : userId;
    postsFilter.from = 0;
    handleOtherPostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedPosts = () => {
    setShowOwnPosts(false);
    setShowOtherPosts(false);
    setShowLikedPosts(!showLikedPosts);
    postsFilter.userId = showLikedPosts ? undefined : userId;
    postsFilter.from = 0;
    handleLikedPostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only other people's posts"
          checked={showOtherPosts}
          onChange={toggleShowOtherPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show posts that I like"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
