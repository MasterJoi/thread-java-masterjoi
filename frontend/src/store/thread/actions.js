import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_ALL_COMMENTS: 'thread/set-all-comments',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const setComments = createAction(ActionType.SET_ALL_COMMENTS, comments => ({
  payload: {
    comments
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadOtherPosts = filter => async dispatch => {
  const posts = await postService.getAllOtherPosts(filter);
  dispatch(setPosts(posts));
};

const loadLikedUserPosts = filter => async dispatch => {
  const posts = await postService.getAllUserPostsByLike(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = (filter, showOwnPosts, showOtherPosts, showLikedPosts) => async (dispatch, getRootState) => {
  console.log(showOwnPosts, showOtherPosts, showLikedPosts);
  const {
    posts: { posts }
  } = getRootState();
  let loadedPosts;
  if (showOwnPosts === true) {
    loadedPosts = await postService.getAllPosts(filter);
  } else if (showOtherPosts === true) {
    loadedPosts = await postService.getAllOtherPosts(filter);
  } else if (showLikedPosts === true) {
    loadedPosts = await postService.getAllUserPostsByLike(filter);
  } else {
    loadedPosts = await postService.getAllPosts(filter);
  }
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  const postReaction = await postService.getReaction(postId);
  let mapLikes;
  if (postReaction === null) {
    mapLikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) + 1
    });
  } else if (postReaction.isDislike === true) {
    mapLikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) + 1,
      dislikeCount: Number(post.dislikeCount) - 1
    });
  } else {
    mapLikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) - 1
    });
  }
  console.log(getRootState());
  await postService.likePost(postId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const postReaction = await postService.getReaction(postId);
  let mapDislikes;
  if (postReaction === null) {
    mapDislikes = post => ({
      ...post,
      dislikeCount: Number(post.dislikeCount) + 1
    });
  } else if (postReaction.isLike === true) {
    mapDislikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) - 1,
      dislikeCount: Number(post.dislikeCount) + 1
    });
  } else {
    mapDislikes = post => ({
      ...post,
      dislikeCount: Number(post.dislikeCount) - 1
    });
  }
  await postService.dislikePost(postId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));
  console.log(updated);
  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const likeComment = commentId => async (dispatch, getRootState) => {
  const commentReaction = await commentService.getCommentReaction(commentId);
  let mapLikes;

  if (commentReaction === null) {
    mapLikes = comment => ({
      ...comment,
      likeCount: Number(comment.likeCount) + 1
    });
  } else if (commentReaction.isDislike === true) {
    mapLikes = comment => ({
      ...comment,
      likeCount: Number(comment.likeCount) + 1,
      dislikeCount: Number(comment.dislikeCount) - 1
    });
  } else {
    mapLikes = comment => ({
      ...comment,
      likeCount: Number(comment.likeCount) - 1
    });
  }
  await commentService.likeComment(commentId);
  const {
    posts: {
      expandedPost: {
        comments
      }
    }
  } = getRootState();

  const updated = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));
  console.log(updated);
  dispatch(setComments(updated));
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const commentReaction = await commentService.getCommentReaction(commentId);
  let mapDislikes;
  if (commentReaction === null) {
    mapDislikes = comment => ({
      ...comment,
      dislikeCount: Number(comment.dislikeCount) + 1
    });
  } else if (commentReaction.isLike === true) {
    mapDislikes = comment => ({
      ...comment,
      likeCount: Number(comment.likeCount) - 1,
      dislikeCount: Number(comment.dislikeCount) + 1
    });
  } else {
    mapDislikes = comment => ({
      ...comment,
      dislikeCount: Number(comment.dislikeCount) - 1
    });
  }
  await commentService.dislikeComment(commentId);

  const {
    comments: { comments }
  } = getRootState();

  const updated = comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));
  dispatch(setPosts(updated));
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  setComments,
  addMorePosts,
  addPost,
  setExpandedPost,
  loadPosts,
  loadOtherPosts,
  loadLikedUserPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  likeComment,
  dislikeComment,
  addComment
};
