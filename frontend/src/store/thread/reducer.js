import { createReducer } from '@reduxjs/toolkit';
import { setPosts, setComments, addMorePosts, addPost, setExpandedPost } from './actions';

const initialPostState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true
};

const initialCommentsState = {
  comments: []
};

const postReducer = createReducer(initialPostState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
});

const commentReducer = createReducer(initialCommentsState, builder => {
  builder.addCase(setComments, (state, action) => {
    const { comments } = action.payload;

    state.comments = comments;
  });
});
export { postReducer, commentReducer };
