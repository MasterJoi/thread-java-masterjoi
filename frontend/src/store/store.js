import { configureStore } from '@reduxjs/toolkit';

import { profileReducer, threadReducer, commentReducer } from './root-reducer';

const store = configureStore({
  reducer: {
    profile: profileReducer,
    posts: threadReducer,
    comments: commentReducer
  }
});

export default store;
