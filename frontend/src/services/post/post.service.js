import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Post {
  constructor({ http }) {
    this._http = http;
  }

  getAllPosts(filter) {
    return this._http.load('/api/posts', {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getAllOtherPosts(filter) {
    return this._http.load('/api/posts/other', {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getAllUserPostsByLike(filter) {
    return this._http.load('/api/posts/liked', {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getAllPostsReactions() {
    return this._http.load('/api/postreaction', {
      method: HttpMethod.GET
    });
  }

  getPost(id) {
    return this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  getReaction(postId) {
    return this._http.load(`/api/postreaction/${postId}`, {
      method: HttpMethod.GET
    });
  }

  addPost(payload) {
    return this._http.load('/api/posts', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likePost(postId) {
    return this._http.load('/api/postreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: true,
        isDislike: false
      })
    });
  }

  dislikePost(postId) {
    return this._http.load('/api/postreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isDislike: true,
        isLike: false
      })
    });
  }
}

export { Post };
