package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class PostReactionService {
    @Autowired
    private PostReactionsRepository postReactionsRepository;

    public Optional<ResponsePostReactionDto> getPostReactionById(UUID userId, UUID postId) {
        var reaction = postReactionsRepository.getPostReaction(userId, postId)
                .map(PostReactionMapper.MAPPER::reactionToPostReactionDto);
        return reaction;
    }

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {

        var reaction = postReactionsRepository.getPostReaction(postReactionDto.getUserId(), postReactionDto.getPostId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == postReactionDto.getIsLike()
                && react.getIsDislike() == postReactionDto.getIsDislike()) {
                postReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(postReactionDto.getIsLike());
                react.setIsDislike(postReactionDto.getIsDislike());
                var result = postReactionsRepository.save(react);
                return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
            }
        } else {
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
            var result = postReactionsRepository.save(postReaction);
            return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
        }
    }
}
